FROM alpine:latest

LABEL maintainer="Kamil Cukrowski <kamilcukrowski@gmail.com>"

ENV VIRTUAL_ENV=/venv
ENV PATH=/venv/bin:$PATH

RUN set -x && \
	apk --no-cache add \
		python3 \
		sqlite \
		sqlite-dev \
		py-setuptools \
		py-babel \
		py-psutil \
		py-future \
		py-pip \
		rdiff-backup \
		libldap \
		py3-pyldap \
		py3-greenlet \
	&& \
	pip install --no-cache-dir  --break-system-packages rdiffweb && \
	rdiffweb --version && \
	rm -rf /usr/share/locale/* && \
	rm -rf /usr/share/man/* && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# we add our default rdw.conf, hope it doesn't change much at upstream, should be overwritten by user anyway
COPY rdw.conf /etc/rdiffweb/rdw.conf
VOLUME ["/etc/rdiffweb"]

EXPOSE 8080

CMD ["/usr/bin/rdiffweb"]

